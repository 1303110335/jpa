package com.itheima.domain;

import com.itheima.utils.JPAUtil;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class JPADemo1 {

    @Test
    public void test1() {
        Customer c = new Customer();
        c.setCustName("JPA Customer2");

        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //执行操作
        em.persist(c);

        tx.commit();

        em.close();
    }

    @Test
    /**
     * 更新操作
     */
    public void test2() {
        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //查询更新的对象，然后执行更新
        Customer c = em.find(Customer.class, 1L);
        System.out.println(c);

        //执行操作
        c.setCustAddress("浙江省余杭区");

        tx.commit();

        em.close();
    }


    @Test
    /**
     * 更新的另一种操作
     */
    public void test4() {
        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //查询更新的对象，然后执行更新
        Customer c = em.find(Customer.class, 1L);
        System.out.println(c);

        //执行操作
        c.setCustAddress("北京市2");

        em.merge(c);

        tx.commit();

        em.close();
    }


    @Test
    /**
     * 删除操作
     */
    public void test5() {
        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //查询更新的对象，然后执行更新
        Customer c = em.find(Customer.class, 1L);
        System.out.println(c);

        //执行操作
        em.remove(c);
        tx.commit();

        em.close();
    }

    @Test
    /**
     * 查询所有
     *
     *  涉及的对象是：
     *      JPA的Query
     *  如何获取该对象
     *      EntityManager 的createQuery(String jpql)
     *  参数的含义：
     *      JPQL:Java Persistence Query Language
     *      它的写法和HQL很相似。也是把表单换成类名，把字段名换成属性名
     *      它在写查询所有时，不能直接写 from 实体类
     *      要使用select 关键字 select c from Customer c
     */
    public void testQueryAll() {
        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Query query = em.createQuery("select c from Customer c where custName like :custName and custLevel = :custLevel ");
        query.setParameter("custName", "%JPA%");
        query.setParameter("custLevel", "2");
        List list = query.getResultList();
        for (Object o : list) {
            System.out.println(o);
        }

        tx.commit();

        em.close();
    }

    @Test
    public void testGetRef() {
        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Customer c = em.getReference(Customer.class, 2L);
        System.out.println(c);

        tx.commit();

        em.close();
    }

}
