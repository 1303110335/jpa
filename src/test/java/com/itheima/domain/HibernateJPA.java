package com.itheima.domain;

import com.itheima.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

/**
 * 使用JPA配置映射
 * 使用hibernate操作者
 */
public class HibernateJPA {

    @Test
    public void test1() {
        Customer c = new Customer();
        c.setCustName("hibernate jpa customer3");

        Session s = HibernateUtils.getCurrentSession();
        Transaction tx = s.beginTransaction();
        s.save(c);
        tx.commit();

    }
}
