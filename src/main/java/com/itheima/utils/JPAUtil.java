package com.itheima.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

    private static EntityManagerFactory factory;

    private static ThreadLocal<EntityManager> threadLocal;

    static {
        factory = Persistence.createEntityManagerFactory("myJPAUnit");
        threadLocal = new ThreadLocal<EntityManager>();
    }

    /*public static EntityManager createEntityManager() {
        return factory.createEntityManager();
    }*/

    //entityManager 绑定线程
    public static EntityManager createEntityManager() {
        //从当前线程获得eneityManager
        EntityManager em = threadLocal.get();
        if (em == null) {
            em = factory.createEntityManager();
            threadLocal.set(em);
        }
        return threadLocal.get();
    }

    public static void main(String[] args) {
        createEntityManager();
    }
}
