package com.itheima.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sys_user")
public class SysUser implements Serializable {

    /**
     * 用户实体类
     */
    @Id
    @Column(name = "user_id")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "user_password")
    private String userPassword;

    @Column(name = "user_state")
    private boolean userState;

    //一个用户可以具备多个角色
    //多对多关系映射：一个角色可以赋予多个用户
    //(cascade = CascadeType.ALL)
    @ManyToMany
    //加入一张表
    @JoinTable(name = "user_role_ref",
            joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "user_id")},//当前实体在中间表的字段
            inverseJoinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "role_id")}//对方实体再中间表的字段
    )
    private Set<SysRole> roles = new HashSet<SysRole>(0);

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public boolean isUserState() {
        return userState;
    }

    public void setUserState(boolean userState) {
        this.userState = userState;
    }

    public Set<SysRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<SysRole> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "SysUser{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userState=" + userState +
                '}';
    }
}
