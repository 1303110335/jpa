package com.itheima.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "sys_role")
public class SysRole implements Serializable {

    @Id
    @Column(name = "role_id")
    @GenericGenerator(name = "uuid", strategy = "uuid") //声明一个主键生成器,name给生成器起个名字 strategy hibernate中包含的生成策略
    @GeneratedValue(generator = "uuid")
    private String roleId;

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "role_memo")
    private String roleMemo;


    //mappedBy 放弃维护关联关系
    @ManyToMany(mappedBy = "roles", cascade = CascadeType.ALL)
    private Set<SysUser> users = new HashSet<SysUser>(0);

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleMemo() {
        return roleMemo;
    }

    public void setRoleMemo(String roleMemo) {
        this.roleMemo = roleMemo;
    }

    public Set<SysUser> getUsers() {
        return users;
    }

    public void setUsers(Set<SysUser> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "roleId=" + roleId +
                ", roleName='" + roleName + '\'' +
                ", roleMemo='" + roleMemo + '\'' +
                '}';
    }
}
