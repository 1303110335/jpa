package com.itheima.domain;

import org.hibernate.Session;
import org.junit.Test;

public class HibernateTest2 {

    @Test
    /**
     * 需求：
     *
     */
    public void testUpdate() {

        //查询一条语句 id=2 (持久态)
        //关闭session     (托管态)

        //再次查询id = 2 (持久态)

        //给对象赋值，修改信息
        //更新托管态的对象 对象信息 oid 与 session中的oid相同，但是对象信息与一级缓存的内容不相同，报错

        //1.最好是更新持久态对象
        //2.merge

    }
}