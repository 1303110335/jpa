package com.itheima.domain;

import com.itheima.utils.JPAUtil;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * jpa 多对多操作
 */
public class Many2Many {

    @Test
    public void testSave() {
        SysUser user1 = new SysUser();
        SysUser user2 = new SysUser();
        user1.setUserName("JPA Many to Many u1");
        user2.setUserName("JPA Many to Many u2");


        SysRole role1 = new SysRole();
        SysRole role2 = new SysRole();
        SysRole role3 = new SysRole();
        role1.setRoleName("JPA Many to Many r1");
        role2.setRoleName("JPA Many to Many r2");
        role3.setRoleName("JPA Many to Many r3");


        //建立用户和角色的关联关系
        user1.getRoles().add(role1);
        user1.getRoles().add(role2);

        user2.getRoles().add(role2);
        user2.getRoles().add(role3);


        role1.getUsers().add(user1);
        role2.getUsers().add(user1);
        role2.getUsers().add(user2);
        role3.getUsers().add(user2);

        EntityManager em = JPAUtil.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //保存
        em.persist(user1);
        em.persist(user2);
        em.persist(role1);
        em.persist(role2);
        em.persist(role3);

        tx.commit();
        em.close();
    }

    @Test
    public void testDelete() {
        EntityManager em = JPAUtil.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        SysUser sysUser = em.find(SysUser.class, "4028b88165183478016518347b080000");
        em.remove(sysUser);
        tx.commit();
        em.close();
    }
}
