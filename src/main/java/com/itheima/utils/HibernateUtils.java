package com.itheima.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtils {
    private static SessionFactory factory;

//    private static ThreadLocal<Session> tl = new ThreadLocal<Session>();

    static {
        try {
            Configuration cfg = new Configuration();
            cfg.configure();
            factory = cfg.buildSessionFactory();
        } catch (ExceptionInInitializerError e) {
            throw new ExceptionInInitializerError("初始化SessionFactory" +
                    "失败，请检查配置文件");
        }
    }

    /**
     * 获取一个新的Session对象
     * 只要使用openSession方法,每次得到的都是一个新的Session
     * @return
     */
    public static Session openSession() {
        /*Session s = tl.get();
        if (s == null) {
            tl.set(factory.openSession());
        }

        return tl.get();*/
        return factory.openSession();
    }

    /**
     * 从当前线程上获取Session对象
     * @return
     */
    public static Session getCurrentSession() {
        //只有设置了current_session_context_class配置，才能得到
        return factory.getCurrentSession();
    }

    public static void main(String[] args) {
        getCurrentSession();
    }
}
