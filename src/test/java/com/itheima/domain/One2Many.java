package com.itheima.domain;

import com.itheima.utils.JPAUtil;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * JPA中一对多的相关操作
 */
public class One2Many {

    /**
     * 保存操作啊
     *  创建一个客户和一个联系人
     *  建立客户和联系人的双向关系
     *  先保存客户，再保存联系人
     */
    @Test
    public void testAdd() {
        Customer c = new Customer();
        LinkMan l = new LinkMan();
        c.setCustName("JPA One To Many Customer");
        l.setLkmName("JPA One To Many LinkMan");

        c.getLinkmans().add(l);
        l.setCustomer(c);

        EntityManager em = JPAUtil.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(c);
        em.persist(l);

        tx.commit();
        em.close();

    }

    /**
     * 更新操作
     */
    @Test
    public void testUpdate() {
        LinkMan l = new LinkMan();
        l.setLkmName("JPA One To Many LinkMan 2");

        EntityManager em = JPAUtil.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Customer c = em.find(Customer.class, 2L);
        c.getLinkmans().add(l);
        l.setCustomer(c);

        tx.commit();
        em.close();

    }

    /**
     * 更新操作
     */
    @Test
    public void testDelete() {
        EntityManager em = JPAUtil.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        Customer c = em.find(Customer.class, 2L);
        em.remove(c);

        tx.commit();
        em.close();

    }

}
