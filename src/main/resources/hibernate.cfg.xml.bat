<?xml version="1.0" encoding="UTF-8" ?>
<!--在类的根路径下创建名称为hibernate.cfg.xml的配置文件-->
<!DOCTYPE hibernate-configuration PUBLIC
        "-//Hibernate/Hibernate Configuration DTD 3.0//EN"
        "http://www.hibernate.org/dtd/hibernate-configuration-3.0.dtd">

<hibernate-configuration>
    <!--配置sessionFactory
        SessionFactory的作用就是创建Session对象的
        Session对象就是hibernate中操作数据库的核心对象
        此处的配置不要求背，但是要求记住创建SessionFactory必须的三部分信息
        1.连接数据库的信息
        2.hibernate的可选配置
        3.映射文件的位置
    -->
    <session-factory>
        <property name="connection.driver_class">com.mysql.cj.jdbc.Driver</property>
        <property name="connection.url">jdbc:mysql://192.168.33.10:3306/hibernate?characterEncoding=UTF-8</property>
        <property name="connection.username">root</property>
        <property name="connection.password">123456</property>
        <property name="dialect">org.hibernate.dialect.MySQL5Dialect</property>
        <!--hibernate的可选配置-->
        <!--print all generated SQL to the console-->
        <property name="show_sql">true</property>
        <!--format SQL in log and console-->
        <property name="format_sql">true</property>
        <!--配置hibernate采用何种方式生成ddl语句-->
        <property name="hibernate.hbm2ddl.auto">update</property><!--update检测实体类的映射配置和数据库的表结构是否一致，若不一致则更新结构-->
        <!--设置c3p0连接池-->
        <property name="hibernate.connection.provider_class">org.hibernate.c3p0.internal.C3P0ConnectionProvider</property>
        <!--SQL结构化查询语言：一共分为6个部分
            DDL:Data Definition Language
            DML:Data Manipulation language  数据操纵语言
            DQL:Data Query Language
            DCL:Data Control Language   数据库控制语言
            CCL:Cursor Control language 游标控制语言
            TPL:Transaction Processing Language 事务处理语言
        -->

        <!--把session和线程绑定，从而使一个线程只有一个Session-->
        <property name="hibernate.current_session_context_class">thread</property>

        <!--第三部分：映射配置文件的位置-->
        <mapping resource="Customer.hbm.xml"></mapping>
        <mapping resource="LinkMan.hbm.xml"></mapping>
        <!--<mapping resource="SysUser.hbm.xml"></mapping>-->
        <!--<mapping resource="SysRole.hbm.xml"></mapping>-->
    </session-factory>
</hibernate-configuration>
