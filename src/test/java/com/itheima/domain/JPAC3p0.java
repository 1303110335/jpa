package com.itheima.domain;

import com.itheima.utils.JPAUtil;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.sql.Connection;
import java.sql.SQLException;

public class JPAC3p0 {
    @Test
    public void test() {

        EntityManager em = JPAUtil.createEntityManager();
        Session session = em.unwrap(Session.class);
        session.doWork(new Work() {
            public void execute(Connection connection) throws SQLException {
                System.out.println(connection.getClass().getName());
            }
        });
    }

    @Test
    public void test2() {
        EntityManager em1 = JPAUtil.createEntityManager();
        EntityManager em2 = JPAUtil.createEntityManager();
        System.out.println(em1 == em2);
    }
}
