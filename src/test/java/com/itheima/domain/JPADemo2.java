package com.itheima.domain;

import com.itheima.utils.JPAUtil;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class JPADemo2 {

    @Test
    /**
     * 使用导航查询
     */
    public void test1() {
        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //执行操作
        Customer c = em.find(Customer.class, 1L);
        System.out.println(c);
        System.out.println(c.getLinkmans());

        tx.commit();

        em.close();
    }

    @Test
    /**
     * 使用导航查询
     */
    public void test2() {
        EntityManager em = JPAUtil.createEntityManager();
        //2.获取事务对象并开启对象
        EntityTransaction tx = em.getTransaction();
        tx.begin();

        //执行操作
        LinkMan linkMan = em.find(LinkMan.class, 1L);
        System.out.println(linkMan);

        Customer c = linkMan.getCustomer();
        System.out.println(c);

        tx.commit();

        em.close();
    }
}
